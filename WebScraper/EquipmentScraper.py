import requests
import json
from bs4 import BeautifulSoup as bs4
from Models.equipmentModel import equipmentModel


class EquipmentScraper:
    itemNames = None
    itemNamesUrl = "https://rsbuddy.com/exchange/names.json"
    equipmentUrl = "https://oldschool.runescape.wiki/w/"

    @staticmethod
    def getItemName(id):
        if (EquipmentScraper.itemNames == None):
            result = requests.get(EquipmentScraper.itemNamesUrl)
            assert result.ok == True
            EquipmentScraper.itemNames = json.loads(result.text)

        # Check if id exists in names
        if (str(id) not in EquipmentScraper.itemNames.keys()):
            print("Item id:", id, "could not be found in names.json")
            return None

        return EquipmentScraper.itemNames[str(id)]["name"]

    @staticmethod
    def getEquipmentStats(id):
        itemName = EquipmentScraper.getItemName(id)

        # Do nothing if item name could not be found
        if (itemName == None):
            return None

        equipmentObj = equipmentModel(id, itemName)
        equipmentObj.url = EquipmentScraper.equipmentUrl+itemName
        print("EquipmentScraper: Fetching", itemName)
        result = requests.get(equipmentObj.url)
        assert result.ok == True

        # Convert to soup
        soup = bs4(result.content, 'html.parser')

        # Find table with stats
        table = soup.find("table", {"class": "infobox-bonuses"})

        ### Get attack bonusses ####
        # Find table row with attack bonuses
        tr_att = table.find_all("tr")[3]
        assert len(tr_att) == 5

        # Find all table data each with a attack bonus
        td_att = tr_att.find_all("td")
        assert len(td_att) == 5

        equipmentObj.attStabBonus = int(td_att[0].text)
        equipmentObj.attSlashBonus = int(td_att[1].text)
        equipmentObj.attCrushBonus = int(td_att[2].text)
        equipmentObj.attMagicBonus = int(td_att[3].text)
        equipmentObj.attRangeBonus = int(td_att[3].text)

        ### Get defense bonusses ####
        # Find table row with defense bonuses
        tr_def = table.find_all("tr")[8]
        assert len(tr_def) == 5

        # Find all table data each with a defense bonus
        td_def = tr_def.find_all("td")
        assert len(td_def) == 5

        equipmentObj.defStabBonus = int(td_def[0].text)
        equipmentObj.defSlashBonus = int(td_def[1].text)
        equipmentObj.defCrushBonus = int(td_def[2].text)
        equipmentObj.defMagicBonus = int(td_def[3].text)
        equipmentObj.defRangeBonus = int(td_def[3].text)

        ### Get item slot ###
        tr_img_slot = table.find_all("tr")[12]
        assert len(tr_img_slot) == 5

        # Fint table header with item slot
        th_img_slot = tr_img_slot.find_all("th")
        assert len(th_img_slot) == 5

        # Find image element
        img_slot = th_img_slot[4].find("img")
        equipmentObj.slot = img_slot["alt"].replace(" slot.png", "")

        ### Get other bonusses ####
        # Find table row with other bonuses
        tr_other = table.find_all("tr")[13]
        assert len(tr_other) == 5

        # Find all table data each with a other bonus
        td_other = tr_other.find_all("td")
        assert len(td_other) == 5

        equipmentObj.strBonus = int(td_other[0].text.replace("%", ""))
        equipmentObj.rangeBonus = int(td_other[1].text.replace("%", ""))
        equipmentObj.magicBonus = int(td_other[2].text.replace("%", ""))
        equipmentObj.prayerBonus = int(td_other[3].text.replace("%", ""))

        return equipmentObj
