from yoyo import step

steps = [
    step(
        """CREATE TABLE equipment 
        (
                id              INTEGER PRIMARY KEY AUTOINCREMENT, 
                name            TEXT NOT NULL,
                defStabBonus    INTEGER,
                defSlashBonus   INTEGER,
                defCrushBonus   INTEGER,
                defMagicBonus   INTEGER,
                defRangeBonus   INTEGER,
                attStabBonus    INTEGER,
                attSlashBonus   INTEGER,
                attCrushBonus   INTEGER,
                attMagicBonus   INTEGER,
                attRangeBonus   INTEGER,
                strBonus        INTEGER,
                magicBonus      INTEGER,
                rangeBonus      INTEGER,
                prayerBonus     INTEGER
        )""", "DROP TABLE equipment"),
]