class dataSampleModel:
    def __init__(self):
        self.id = -1
        self.lineNumber = 0
        self.fightID = -1
        self.fightDuration = -1
        self.timeRecorded = -1

        self.fightState = -1
        self.winnerPlayer = -1
        self.fleerPlayer = -1

        self.player1 = -1
        self.player2 = -1

        self.p1_tileX = -1
        self.p1_tileY = -1
        self.p1_tileZ = -1
        self.p1_isSkulled = False
        self.p1_healthLeft = -1
        self.p1_weaponID = -1
        self.p1_hatID = -1
        self.p1_amuletID = -1
        self.p1_capeID = -1
        self.p1_chestID = -1
        self.p1_feetID = -1
        self.p1_legsID = -1
        self.p1_shieldID = -1
        self.p1_handsID = -1

        self.p2_tileX = -1
        self.p2_tileY = -1
        self.p2_tileZ = -1
        self.p2_isSkulled = False
        self.p2_healthLeft = -1
        self.p2_weaponID = -1
        self.p2_hatID = -1
        self.p2_amuletID = -1
        self.p2_capeID = -1
        self.p2_chestID = -1
        self.p2_feetID = -1
        self.p2_legsID = -1
        self.p2_shieldID = -1
        self.p2_handsID = -1
