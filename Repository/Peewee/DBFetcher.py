from Repository.Peewee.peeweetest import Player
from Repository.Peewee.peeweetest import Equipment
from Repository.Peewee.peeweetest import DataSample


def getPlayerByName(name):
    query = Player.select().where(Player.playerName == name)

    if not query.exists():
        print("ERROR: Failed to fetch player({}): Reason Query did not exist".format(name))
        return None

    return query.first()


def getEquipmentById(id):
    query = Equipment.select().where(Equipment.id == id)

    if not query.exists():
        print("ERROR: Failed to fetch equipment({}): Reason Query did not exist".format(id))
        return None

    return query.first()


def getDataSampleById(id):
    query = DataSample.select().where(DataSample.id == id)
    if not query.exists():
        print(
            "ERROR: Failed to fetch datasample({}): Reason Query did not exist".format(id))
        return None

    return query.first()


def getAllFightIds():
    query = DataSample.select(DataSample.fightID).distinct()

    if not query.exists():
        print("ERROR: Failed to get fightIds: Reason Query did not exist")
        return None

    return query


def getEndFightByFightID(fightID):
    query = DataSample.select().where(
        (DataSample.fightID == fightID) &
        ((DataSample.winnerPlayer.is_null(False)) |
         (DataSample.fleerPlayer.is_null(False)))
    )

    if not query.exists():
        print("ERROR: Failed to fetch datasample by fightID({}): Reason Query did not exist".format(fightID))
        return None

    return query.first()


def getWholeFightById(fightID):
    query = DataSample.select().where(DataSample.fightID == fightID)

    if not query.exists():
        print("ERROR: Failed to fetch datasample by fightID({}): Reason Query did not exist".format(fightID))
        return None

    return query
