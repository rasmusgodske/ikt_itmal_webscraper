class equipmentModel:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.url = ""
        self.slot = ""
        self.defStabBonus = 0
        self.defSlashBonus = 0
        self.defCrushBonus = 0
        self.defMagicBonus = 0
        self.defRangeBonus = 0
        self.attStabBonus = 0
        self.attSlashBonus = 0
        self.attCrushBonus = 0
        self.attMagicBonus = 0
        self.attRangeBonus = 0
        self.strBonus = 0
        self.rangeBonus = 0
        self.magicBonus = 0
        self.prayerBonus = 0
