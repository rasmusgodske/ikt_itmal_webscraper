import requests
from Models.playerModel import playerModel


class PlayerScraper:
    highscoreURL = "https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player="
    colnames = [
        'overall_rank', 'overall_level', 'overall_xp',
        'att_rank', 'att_level', 'att_xp',
        'def_rank', 'def_level', 'def_xp',
        'str_rank', 'str_level', 'str_xp',
        'hp_rank', 'hp_level', 'hp_xp',
        'range_rank', 'range_level', 'range_xp',
        'prayer_rank', 'prayer_level', 'prayer_xp',
        'magic_rank', 'magic_level', 'magic_xp',
    ]

    def scrape(playerName):
        result = requests.get(PlayerScraper.highscoreURL+playerName)
        if result.ok != True:
            return None

        onlineResult = result.text.replace("\n", ",")   # Remove new lines
        onlineResultArr = onlineResult.split(",")       # Convert to array

        # Save to variables
        att_level = onlineResultArr[PlayerScraper.colnames.index("att_level")]
        def_level = onlineResultArr[PlayerScraper.colnames.index("def_level")]
        str_level = onlineResultArr[PlayerScraper.colnames.index("str_level")]
        hp_level = onlineResultArr[PlayerScraper.colnames.index("hp_level")]
        range_level = onlineResultArr[PlayerScraper.colnames.index(
            "range_level")]
        prayer_level = onlineResultArr[PlayerScraper.colnames.index(
            "prayer_level")]
        magic_level = onlineResultArr[PlayerScraper.colnames.index(
            "magic_level")]

        print("Att   :", att_level)
        print("Def   :", def_level)
        print("Str   :", str_level)
        print("HP    :", hp_level)
        print("Range :", range_level)
        print("Prayer:", prayer_level)
        print("Magic :", magic_level)

        player = playerModel(playerName)
        player.attLevel = int(att_level)
        player.str_level = int(str_level)
        player.defLevel = int(def_level)
        player.healthLevel = int(hp_level)
        player.rangeLevel = int(range_level)
        player.prayerLevel = int(prayer_level)
        player.magicLevel = int(magic_level)

        return player


# PlayerScraper.test("Baseball1583")
