from yoyo import step

steps = [
    step(
        """CREATE TABLE player 
        (
                id              INTEGER PRIMARY KEY AUTOINCREMENT, 
                playerName      TEXT NOT NULL,
                combatLevel     INTEGER,
                strLevel        INTEGER,
                attLevel        INTEGER,
                rangeLevel      INTEGER,
                defLevel        INTEGER,
                magicLevel      INTEGER,
                prayerLevel     INTEGER,
                healthLevel     INTEGER
        )""", "DROP TABLE player"),
]