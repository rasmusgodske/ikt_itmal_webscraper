#!"d:\programmering\rsos bots\dreambot\itmal datacollection\webscraper\.venv\scripts\python.exe"
# EASY-INSTALL-ENTRY-SCRIPT: 'iniherit==0.3.9','console_scripts','iniherit'
__requires__ = 'iniherit==0.3.9'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('iniherit==0.3.9', 'console_scripts', 'iniherit')()
    )
