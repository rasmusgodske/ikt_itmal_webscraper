import datetime
from peewee import *
from Models.equipmentModel import equipmentModel
from Models.dataSampleModel import dataSampleModel
from Models.playerModel import playerModel

db = SqliteDatabase('db/my_app.sqlite')


class BaseModel(Model):
    class Meta:
        database = db


class Player(BaseModel):
    playerName = CharField(primary_key=True)
    combatLevel = IntegerField()
    strLevel = IntegerField()
    attLevel = IntegerField()
    defLevel = IntegerField()
    rangeLevel = IntegerField()
    magicLevel = IntegerField()
    prayerLevel = IntegerField()
    healthLevel = IntegerField()

    def toModel(PlayerQuery):
        newPlayerModel = playerModel(PlayerQuery.playerName)
        
        newPlayerModel.combatLevel  = PlayerQuery.combatLevel
        newPlayerModel.strLevel     = PlayerQuery.strLevel
        newPlayerModel.attLevel     = PlayerQuery.attLevel
        newPlayerModel.defLevel     = PlayerQuery.defLevel
        newPlayerModel.rangeLevel   = PlayerQuery.rangeLevel
        newPlayerModel.magicLevel   = PlayerQuery.magicLevel
        newPlayerModel.prayerLevel  = PlayerQuery.prayerLevel
        newPlayerModel.healthLevel  = PlayerQuery.healthLevel
        return newPlayerModel

class Equipment(BaseModel):
    id = IntegerField(primary_key=True)
    name = CharField()
    url = CharField()
    slot = CharField()
    defStabBonus = IntegerField()
    defSlashBonus = IntegerField()
    defCrushBonus = IntegerField()
    defMagicBonus = IntegerField()
    defRangeBonus = IntegerField()
    attStabBonus = IntegerField()
    attSlashBonus = IntegerField()
    attCrushBonus = IntegerField()
    attMagicBonus = IntegerField()
    attRangeBonus = IntegerField()
    strBonus = IntegerField()
    rangeBonus = IntegerField()
    magicBonus = IntegerField()
    prayerBonus = IntegerField()
    
    def toModel(Query):
        Equipment = equipmentModel(Query.id, Query.name)
        
        Equipment.url           = Query.url
        Equipment.slot          = Query.slot
        Equipment.defStabBonus  = Query.defStabBonus
        Equipment.defSlashBonus = Query.defSlashBonus
        Equipment.defCrushBonus = Query.defCrushBonus
        Equipment.defMagicBonus = Query.defMagicBonus
        Equipment.defRangeBonus = Query.defRangeBonus
        Equipment.attStabBonus  = Query.attStabBonus
        Equipment.attSlashBonus = Query.attSlashBonus
        Equipment.attCrushBonus = Query.attCrushBonus
        Equipment.attMagicBonus = Query.attMagicBonus
        Equipment.attRangeBonus = Query.attRangeBonus
        Equipment.strBonus      = Query.strBonus
        Equipment.rangeBonus    = Query.rangeBonus
        Equipment.magicBonus    = Query.magicBonus
        Equipment.prayerBonus   = Query.prayerBonus
        
        return Equipment

class DataSample(BaseModel):
    id = IntegerField(primary_key=True)
    lineNumber = IntegerField(unique=True)
    fightID = IntegerField()
    fightDuration = DecimalField()
    timeRecorded = DateTimeField()

    fightState = IntegerField()
    winnerPlayer = ForeignKeyField(Player, to_field="playerName", null=True)
    fleerPlayer = ForeignKeyField(Player, to_field="playerName", null=True)

    player1 = ForeignKeyField(Player, to_field="playerName", null=True)
    player2 = ForeignKeyField(Player, to_field="playerName", null=True)

    p1_tileX = IntegerField()
    p1_tileY = IntegerField()
    p1_tileZ = IntegerField()
    p1_isSkulled = BooleanField()
    p1_healthLeft = IntegerField()
    p1_weaponID = ForeignKeyField(Equipment, to_field="id", null=True)
    p1_hatID = ForeignKeyField(Equipment, to_field="id", null=True)
    p1_amuletID = ForeignKeyField(Equipment, to_field="id", null=True)
    p1_capeID = ForeignKeyField(Equipment, to_field="id", null=True)
    p1_chestID = ForeignKeyField(Equipment, to_field="id", null=True)
    p1_feetID = ForeignKeyField(Equipment, to_field="id", null=True)
    p1_legsID = ForeignKeyField(Equipment, to_field="id", null=True)
    p1_shieldID = ForeignKeyField(Equipment, to_field="id", null=True)
    p1_handsID = ForeignKeyField(Equipment, to_field="id", null=True)

    p2_tileX = IntegerField()
    p2_tileY = IntegerField()
    p2_tileZ = IntegerField()
    p2_isSkulled = BooleanField()
    p2_healthLeft = IntegerField()
    p2_weaponID = ForeignKeyField(Equipment, to_field="id", null=True)
    p2_hatID = ForeignKeyField(Equipment, to_field="id", null=True)
    p2_amuletID = ForeignKeyField(Equipment, to_field="id", null=True)
    p2_capeID = ForeignKeyField(Equipment, to_field="id", null=True)
    p2_chestID = ForeignKeyField(Equipment, to_field="id", null=True)
    p2_feetID = ForeignKeyField(Equipment, to_field="id", null=True)
    p2_legsID = ForeignKeyField(Equipment, to_field="id", null=True)
    p2_shieldID = ForeignKeyField(Equipment, to_field="id", null=True)
    p2_handsID = ForeignKeyField(Equipment, to_field="id", null=True)

    def toModel(Query):
        sample = dataSampleModel()
        
        sample.id               = Query.id
        sample.lineNumber       = Query.lineNumber
        sample.fightID          = Query.fightID
        sample.fightDuration    = Query.fightDuration
        sample.timeRecorded     = Query.timeRecorded
        
        sample.fightState   = Query.fightState
        sample.winnerPlayer = Query.winnerPlayer
        sample.fleerPlayer  = Query.fleerPlayer
        
        sample.player1  = Player.toModel(Query.player1)
        sample.player2  = Player.toModel(Query.player2)
        
        sample.p1_tileX         = Query.p1_tileX
        sample.p1_tileY         = Query.p1_tileY
        sample.p1_tileZ         = Query.p1_tileZ
        sample.p1_isSkulled     = Query.p1_isSkulled
        sample.p1_healthLeft    = Query.p1_healthLeft
        sample.p1_weaponID      = Equipment.toModel(Query.p1_weaponID)
        sample.p1_hatID         = Equipment.toModel(Query.p1_hatID)
        sample.p1_amuletID      = Equipment.toModel(Query.p1_amuletID)
        sample.p1_capeID        = Equipment.toModel(Query.p1_capeID)
        sample.p1_chestID       = Equipment.toModel(Query.p1_chestID)
        sample.p1_feetID        = Equipment.toModel(Query.p1_feetID)
        sample.p1_legsID        = Equipment.toModel(Query.p1_legsID)
        sample.p1_shieldID      = Equipment.toModel(Query.p1_shieldID)
        sample.p1_handsID       = Equipment.toModel(Query.p1_handsID)
        
        sample.p2_tileY         = Query.p2_tileY
        sample.p2_tileZ         = Query.p2_tileZ
        sample.p2_tileX         = Query.p2_tileX
        sample.p2_isSkulled     = Query.p2_isSkulled
        sample.p2_healthLeft    = Query.p2_healthLeft
        sample.p2_weaponID      = Equipment.toModel(Query.p2_weaponID)
        sample.p2_hatID         = Equipment.toModel(Query.p2_hatID)
        sample.p2_amuletID      = Equipment.toModel(Query.p2_amuletID)
        sample.p2_capeID        = Equipment.toModel(Query.p2_capeID)
        sample.p2_chestID       = Equipment.toModel(Query.p2_chestID)
        sample.p2_feetID        = Equipment.toModel(Query.p2_feetID)
        sample.p2_legsID        = Equipment.toModel(Query.p2_legsID)
        sample.p2_shieldID      = Equipment.toModel(Query.p2_shieldID)
        sample.p2_handsID       = Equipment.toModel(Query.p2_handsID)
        
        return sample
        
        
def createDB():
    db.create_tables([Player, Equipment, DataSample])
    
    if not database.equipmentExists(-1):
        noEquipment = Equipment.create(
            id = -1,
            name = "empty",
            url = "none",
            slot = "None",
            defStabBonus = 0,
            defCrushBonus = 0,
            defSlashBonus = 0,
            defMagicBonus = 0,
            defRangeBonus = 0,
            attStabBonus = 0,
            attSlashBonus = 0,
            attCrushBonus = 0,
            attMagicBonus = 0,
            attRangeBonus = 0,
            strBonus = 0,
            rangeBonus = 0,
            magicBonus = 0,
            prayerBonus = 0
        )
        noEquipment.save()


class database:
    @staticmethod
    def playerExists(playerName):
        query = Player.select().where(Player.playerName == playerName)
        return query.exists()

    @staticmethod
    def createPlayer(playerName, combatLevel, strLevel, attLevel, defLevel, rangeLevel, prayerLevel, magicLevel, healthLevel):
        player = Player.create(playerName=playerName,
                               combatLevel=combatLevel,
                               strLevel=strLevel,
                               attLevel=attLevel,
                               defLevel=defLevel,
                               rangeLevel=rangeLevel,
                               prayerLevel=prayerLevel,
                               magicLevel=magicLevel,
                               healthLevel=healthLevel)
        print("Database: Added", player.playerName)
        player.save()

    @staticmethod
    def equipmentExists(id):
        query = Equipment.select().where(Equipment.id == id)
        return query.exists()

    @staticmethod
    def createEquipment(equipModel):
        equipment = Equipment.create(id=equipModel.id,
                                     name=equipModel.name,
                                     url=equipModel.url,
                                     slot=equipModel.slot,
                                     defStabBonus=equipModel.defStabBonus,
                                     defSlashBonus=equipModel.defSlashBonus,
                                     defCrushBonus=equipModel.defCrushBonus,
                                     defMagicBonus=equipModel.defMagicBonus,
                                     defRangeBonus=equipModel.defRangeBonus,
                                     attStabBonus=equipModel.attStabBonus,
                                     attSlashBonus=equipModel.attSlashBonus,
                                     attCrushBonus=equipModel.attCrushBonus,
                                     attMagicBonus=equipModel.attMagicBonus,
                                     attRangeBonus=equipModel.attRangeBonus,
                                     strBonus=equipModel.strBonus,
                                     rangeBonus=equipModel.rangeBonus,
                                     magicBonus=equipModel.magicBonus,
                                     prayerBonus=equipModel.prayerBonus
                                     )
        print("Database: Added", equipment.name)
        equipment.save()
    
    @staticmethod
    def getNewFightID():
        # Fetch datasamples sorted by highest value
        query = DataSample.select(fn.MAX(DataSample.fightID))
        newFightID = 0
        
        # Check if any was returned
        if query.count() != 0:
            # Get the fightID of the highest
            newFightID = query.scalar() +1
        
        return newFightID
    
    @staticmethod
    def getLastDataSample():
        lineNumber = database.getLineNumber()
        print("LineNumber:", lineNumber)
        query = (DataSample.select()
                    .where(DataSample.lineNumber == lineNumber))
                    
                    
        
        # Check if record exists
        if not query.exists():
            print("no row")
            return None
        
        record = query.first()
        # print("state:", record.fightState)
        # print("p1   :", Player.toModel(record.player1).playerName)
        # print("p2   :", Player.toModel(record.player2).playerName)
        # print("Equip:", record.p1_feetID)
        
        # print("id   :", record.id)
        
        
        
        return DataSample.toModel(record)
    
    @staticmethod
    def getLineNumber():
        # Fetch datasamples sorted by highest value
        query = DataSample.select(fn.MAX(DataSample.lineNumber))
        lineNumber = 0
        
        # Check if any was returned
        if query.count() != 0:
            # Get the lineNumber of the highest record
            lineNumber = query.scalar()
        
        return lineNumber
    
    
    @staticmethod
    def dataSampleExists(sampleModel):
        query = DataSample.select().where(
            DataSample.fightDuration == sampleModel.fightDuration and
            DataSample.timeRecorded == sampleModel.timeRecorded and
            DataSample.lineNumber == sampleModel.lineNumber)
        return query.exists()

    @staticmethod
    def createDataSample(sampleModel):
        dataSample = DataSample.create(
            fightID = sampleModel.fightID,

            lineNumber = sampleModel.lineNumber,
            fightDuration   = sampleModel.fightDuration,
            timeRecorded    = sampleModel.timeRecorded,

            fightState      = sampleModel.fightState,
            winnerPlayer    = sampleModel.winnerPlayer,
            fleerPlayer     = sampleModel.fleerPlayer,
        
            player1     = sampleModel.player1,
            player2     = sampleModel.player2,

            p1_tileX        = sampleModel.p1_tileX,
            p1_tileY        = sampleModel.p1_tileY,
            p1_tileZ        = sampleModel.p1_tileZ,
            p1_isSkulled    = sampleModel.p1_isSkulled,
            p1_healthLeft   = sampleModel.p1_healthLeft,
            p1_weaponID     = sampleModel.p1_weaponID,
            p1_hatID        = sampleModel.p1_hatID,
            p1_amuletID     = sampleModel.p1_amuletID,
            p1_capeID       = sampleModel.p1_capeID,
            p1_chestID      = sampleModel.p1_chestID,
            p1_feetID       = sampleModel.p1_feetID,
            p1_legsID       = sampleModel.p1_legsID,
            p1_shieldID     = sampleModel.p1_shieldID,
            p1_handsID      = sampleModel.p1_handsID,

            p2_tileX        = sampleModel.p2_tileX,
            p2_tileY        = sampleModel.p2_tileY,
            p2_tileZ        = sampleModel.p2_tileZ,
            p2_isSkulled    = sampleModel.p2_isSkulled,
            p2_healthLeft   = sampleModel.p2_healthLeft,
            p2_weaponID     = sampleModel.p2_weaponID,
            p2_hatID        = sampleModel.p2_hatID,
            p2_amuletID     = sampleModel.p2_amuletID,
            p2_capeID       = sampleModel.p2_capeID,
            p2_chestID      = sampleModel.p2_chestID,
            p2_feetID       = sampleModel.p2_feetID,
            p2_legsID       = sampleModel.p2_legsID,
            p2_shieldID     = sampleModel.p2_shieldID,
            p2_handsID      = sampleModel.p2_handsID
        )
        
        dataSample.save()
