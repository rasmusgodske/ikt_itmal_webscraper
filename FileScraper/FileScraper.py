import csv


class FileScraper:
    def __init__(self, filePath, lineNumber):
        self.filePath = filePath
        #self.lineNumber = 84619
        self.rowChunk = []
        self.chunkSize = 10000

        self.lineNumber = lineNumber
        self.colnames = ['sampleTime', 'fightDuration',
                         'fightState', 'winnerPlayer',
                         'fleerplayer', 'p1_username',
                         'p1_combatLevel', 'p1_health',
                         'p1_isSkulled', 'p1_tileX',
                         'p1_tileY', 'p1_tileZ',
                         'p1_weaponID', 'p1_hatID',
                         'p1_amuletID', 'p1_capeID',
                         'p1_chestID', 'p1_feetID',
                         'p1_legsID', 'p1_shieldID',
                         'p1_handsID', 'p2_username',
                         'p2_combatLevel', 'p2_health',
                         'p2_isSkulled', 'p2_tileX',
                         'p2_tileY', 'p2_tileZ',
                         'p2_weaponID', 'p2_hatID',
                         'p2_amuletID', 'p2_capeID',
                         'p2_chestID', 'p2_feetID',
                         'p2_legsID', 'p2_shieldID',
                         'p2_handsID']

    def getLineNumber(self):
        return self.lineNumber

    def readLine(self):
        if len(self.rowChunk) > 0:
            self.lineNumber += 1
            return self.rowChunk.pop(0)

        with open(self.filePath, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file, fieldnames=self.colnames)
            line_count = 0
            for row in csv_reader:
                line_count += 1
                if (line_count < self.lineNumber-1):
                    continue

                if len(self.rowChunk) < self.chunkSize:
                    self.rowChunk.append(row)
                else:
                    print("Lines added")
                    break
            self.lineNumber += 1
            return self.rowChunk.pop(0)


# scraper = FileScraper("Data\datasamples.txt")

# print(scraper.readLine())
