from Repository.Peewee.peeweetest import database
from Repository.Peewee.peeweetest import createDB
from FileScraper.FileScraper import FileScraper
from WebScraper.PlayerScraper import PlayerScraper
from WebScraper.EquipmentScraper import EquipmentScraper
from Models.dataSampleModel import dataSampleModel
from Models.equipmentModel import equipmentModel

import time


class dataCollector:

    def __init__(self):
        # Create database if does not exist
        createDB()
        self.handledPlayerNames = []
        self.handledEquipmentIDs = []

    def handlePlayer(self, playerName, combatlevel):
        if (playerName in self.handledPlayerNames):
            return True

        # Check if player already exist in database
        if database.playerExists(playerName):
            print(playerName, "already exist in database")
            self.handledPlayerNames.append(playerName)
            return True

        # Get player
        result = PlayerScraper.scrape(playerName)
        
        # If player data could not be fetched add a empty one indicating player wasn't found
        if result == None:
            print("Failed to find player:", playerName)
            self.handledPlayerNames.append(playerName)
            database.createPlayer(playerName=playerName,
                                    combatLevel=-1,
                                    strLevel=-1,
                                    attLevel=-1,
                                    defLevel=-1,
                                    rangeLevel=-1,
                                    prayerLevel=-1,
                                    magicLevel=-1,
                                    healthLevel=-1)
            return False

        result.playerName = playerName
        database.createPlayer(playerName=playerName,
                              combatLevel=combatlevel,
                              strLevel=result.strLevel,
                              attLevel=result.attLevel,
                              defLevel=result.defLevel,
                              rangeLevel=result.rangeLevel,
                              prayerLevel=result.prayerLevel,
                              magicLevel=result.magicLevel,
                              healthLevel=result.healthLevel)

        self.handledPlayerNames.append(playerName)
        return True

    def handleEquipmentID(self, id):
        # Do nothing if it's already scanned
        if (id in self.handledEquipmentIDs):
                return

        # Do nothing if id is -1
        if (int(id) == -1):
            return

        # Do nothing if it already exists in database
        if (database.equipmentExists(id)):
            return 

        # Fetch equipment data
        result = None
        try:
            result = EquipmentScraper.getEquipmentStats(id)
            print("RESULT ID: ", result.id)
        except:
            print("Failed to fetch item", id)
            result = equipmentModel(id, "failed to load")                    

        database.createEquipment(result)
        self.handledEquipmentIDs.append(id)            

    def handleDataSample(self, Line, fightID, lineNumber):
        DataSample = dataSampleModel()
        DataSample.lineNumber       = lineNumber
        DataSample.fightID          = fightID
        DataSample.fightDuration    = Line["fightDuration"]
        DataSample.timeRecorded     = Line["sampleTime"]
        DataSample.fightState       = Line["fightState"]
        DataSample.winnerPlayer     = Line["winnerPlayer"]  if Line["winnerPlayer"] != "" else None
        DataSample.fleerPlayer      = Line["fleerplayer"]   if Line["fleerplayer"] != "" else None
        
        DataSample.player1 = Line["p1_username"]    if "p1_username" in Line != "" else None
        DataSample.player2 = Line["p2_username"]    if "p2_username" in Line != "" else None

        DataSample.p1_tileX         = Line["p1_tileX"] 
        DataSample.p1_tileY         = Line["p1_tileY"] 
        DataSample.p1_tileZ         = Line["p1_tileZ"] 
        DataSample.p1_isSkulled     = Line["p1_isSkulled"] 
        DataSample.p1_healthLeft    = Line["p1_health"] 
 
        DataSample.p1_weaponID  = Line["p1_weaponID"]   if Line["p1_weaponID"]  != "" else None
        DataSample.p1_hatID     = Line["p1_hatID"]      if Line["p1_hatID"]     != "" else None
        DataSample.p1_amuletID  = Line["p1_amuletID"]   if Line["p1_amuletID"]  != "" else None
        DataSample.p1_capeID    = Line["p1_capeID"]     if Line["p1_capeID"]    != "" else None
        DataSample.p1_chestID   = Line["p1_chestID"]    if Line["p1_chestID"]   != "" else None
        DataSample.p1_feetID    = Line["p1_feetID"]     if Line["p1_feetID"]    != "" else None
        DataSample.p1_legsID    = Line["p1_legsID"]     if Line["p1_legsID"]    != "" else None
        DataSample.p1_shieldID  = Line["p1_shieldID"]   if Line["p1_shieldID"]  != "" else None
        DataSample.p1_handsID   = Line["p1_handsID"]    if Line["p1_handsID"]   != "" else None    

        DataSample.p2_tileX         = Line["p2_tileX"]
        DataSample.p2_tileY         = Line["p2_tileY"]
        DataSample.p2_tileZ         = Line["p2_tileZ"]
        DataSample.p2_isSkulled     = Line["p2_isSkulled"] 
        DataSample.p2_healthLeft    = Line["p2_health"]

        DataSample.p2_weaponID  = Line["p2_weaponID"]   if Line["p2_weaponID"]  != "" else None
        DataSample.p2_hatID     = Line["p2_hatID"]      if Line["p2_hatID"]     != "" else None
        DataSample.p2_amuletID  = Line["p2_amuletID"]   if Line["p2_amuletID"]  != "" else None
        DataSample.p2_capeID    = Line["p2_capeID"]     if Line["p2_capeID"]    != "" else None
        DataSample.p2_chestID   = Line["p2_chestID"]    if Line["p2_chestID"]   != "" else None
        DataSample.p2_feetID    = Line["p2_feetID"]     if Line["p2_feetID"]    != "" else None
        DataSample.p2_legsID    = Line["p2_legsID"]     if Line["p2_legsID"]    != "" else None
        DataSample.p2_shieldID  = Line["p2_shieldID"]   if Line["p2_shieldID"]  != "" else None
        DataSample.p2_handsID   = Line["p2_handsID"]    if Line["p2_handsID"]   != "" else None
        
        if not database.dataSampleExists(DataSample):
            database.createDataSample(DataSample)

    def readFile(self):
        lineNumber = database.getLineNumber()
        lineNumber = 1
        fightID = -1
        
        # Required inorder to check if it's the same fight that is going on
        lastP1Name = None   # player 1 name
        lastP2Name = None   # Player 2 name
        
        lastDataSample = None
        try:
            lastDataSample = database.getLastDataSample()
        except:
            print("failed to get last datasample")
        
            
        if lastDataSample != None:
            lastP1Name  = lastDataSample.player1.playerName
            lastP2Name  = lastDataSample.player2.playerName
            fightID     = lastDataSample.fightID
            print("Found last record...")
        
        print("player1: ", lastP1Name)
        print("player2: ", lastP2Name)
        print("fightID: ", fightID)
        
        fileScraper = FileScraper("Data\datasamples - backup 4.txt", lineNumber)
        
        
        while True:
            start = time.process_time_ns()
            Line = fileScraper.readLine()
            
            # Handle Equipment
            self.handleEquipmentID(Line["p1_weaponID"])
            self.handleEquipmentID(Line["p1_hatID"])
            self.handleEquipmentID(Line["p1_amuletID"])
            self.handleEquipmentID(Line["p1_capeID"])
            self.handleEquipmentID(Line["p1_chestID"])
            self.handleEquipmentID(Line["p1_feetID"])
            self.handleEquipmentID(Line["p1_legsID"])
            self.handleEquipmentID(Line["p1_shieldID"])
            self.handleEquipmentID(Line["p1_handsID"])
            self.handleEquipmentID(Line["p2_weaponID"])
            self.handleEquipmentID(Line["p2_hatID"])
            self.handleEquipmentID(Line["p2_amuletID"])
            self.handleEquipmentID(Line["p2_capeID"])
            self.handleEquipmentID(Line["p2_chestID"])
            self.handleEquipmentID(Line["p2_feetID"])
            self.handleEquipmentID(Line["p2_legsID"])
            self.handleEquipmentID(Line["p2_shieldID"])
            self.handleEquipmentID(Line["p2_handsID"])

            # Handle players
            self.handlePlayer(Line["p1_username"], Line["p1_combatLevel"])
            self.handlePlayer(Line["p2_username"], Line["p2_combatLevel"])

            # Handle Player
            self.handleDataSample(Line, fightID, fileScraper.getLineNumber())
            
            stop = time.process_time_ns()
            msSpent = (stop - start) / 1000000
            
            print("Scanning line: {:6d},\t {:.5f}ms"\
                    ".\tfightID {} - {} vs {}".format(
                        fileScraper.getLineNumber(), 
                        round(msSpent,2),
                        fightID,
                        Line["p1_username"],
                        Line["p2_username"]
                        ))
            
            
            # Get new fightID if new fight is found
            if lastP1Name != Line["p1_username"] or lastP2Name != Line["p2_username"]:
                fightID = database.getNewFightID()
            
            lastP1Name = Line["p1_username"]
            lastP2Name = Line["p2_username"]

    def start(self):
        self.readFile()
        # result = EquipmentScraper.getEquipmentStats(4405)
        # result2 = self.handleEquipmentID(4405)
        # print(  "id: {} \n"\
        #         "name: {}\n"\
        #         "url: {}\n"
        #         .format(result.id, result.name, result.url))
        # print(result)

collector = dataCollector()

collector.start()
# collector.readFile()
print("started")

# if not (database.playerExists("Gheotic")):
#     database.createPlayer("Gheotic", 32, 42, 20, 1, 10, 42, 1, 100)
