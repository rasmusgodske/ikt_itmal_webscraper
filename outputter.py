import time
import datetime
from Repository.Peewee.DBFetcher import getPlayerByName
from Repository.Peewee.DBFetcher import getEquipmentById
from Repository.Peewee.DBFetcher import getDataSampleById
from Repository.Peewee.DBFetcher import getEndFightByFightID
from Repository.Peewee.DBFetcher import getWholeFightById
from Repository.Peewee.DBFetcher import getAllFightIds


class playerValidator:
    checkedPlayers = {}

    @staticmethod
    def validate(playerid):
        if playerid in playerValidator.checkedPlayers.keys():
            return playerValidator.checkedPlayers[playerid]

        result = getPlayerByName(playerid)
        isValid = result != None
        playerValidator.checkedPlayers[playerid] = isValid
        return isValid


class equipmentValidator:
    checkedEquipment = {}

    @staticmethod
    def validate(equipmentId):
        if equipmentId in equipmentValidator.checkedEquipment.keys():
            return equipmentValidator.checkedEquipment[equipmentId]

        result = getEquipmentById(equipmentId)
        isValid = result != None
        equipmentValidator.checkedEquipment[equipmentId] = isValid
        return isValid


class sampleValidator:
    def validate(sampleid):
        sample = getDataSampleById(sampleid)
        # Check if sample exists
        if sample == None:
            return False

        # Check players
        if not playerValidator.validate(sample.player1):
            return False
        if not playerValidator.validate(sample.player2):
            return False

        # Check player 1 equipment
        if not equipmentValidator.validate(sample.p1_weaponID):
            return False
        if not equipmentValidator.validate(sample.p1_hatID):
            return False
        if not equipmentValidator.validate(sample.p1_amuletID):
            return False
        if not equipmentValidator.validate(sample.p1_capeID):
            return False
        if not equipmentValidator.validate(sample.p1_chestID):
            return False
        if not equipmentValidator.validate(sample.p1_feetID):
            return False
        if not equipmentValidator.validate(sample.p1_legsID):
            return False
        if not equipmentValidator.validate(sample.p1_shieldID):
            return False
        if not equipmentValidator.validate(sample.p1_handsID):
            return False

        # Check player 2 equipment
        if not equipmentValidator.validate(sample.p2_weaponID):
            return False
        if not equipmentValidator.validate(sample.p2_hatID):
            return False
        if not equipmentValidator.validate(sample.p2_amuletID):
            return False
        if not equipmentValidator.validate(sample.p2_capeID):
            return False
        if not equipmentValidator.validate(sample.p2_chestID):
            return False
        if not equipmentValidator.validate(sample.p2_feetID):
            return False
        if not equipmentValidator.validate(sample.p2_legsID):
            return False
        if not equipmentValidator.validate(sample.p2_shieldID):
            return False
        if not equipmentValidator.validate(sample.p2_handsID):
            return False

        return True


def validEquipment(sample):
    response = {
        "status": False,
        "funcName": "validEquipment",
        "Reason": ""
    }

    def equipmentScrapeSucceded(equipment):
        if (equipment.name == "failed to load"):
            # print("Invalid item")
            return False
        else:
            return True

    equipmentList = [
        sample.p1_weaponID,
        sample.p1_hatID,
        sample.p1_amuletID,
        sample.p1_capeID,
        sample.p1_chestID,
        sample.p1_feetID,
        sample.p1_legsID,
        sample.p1_shieldID,
        sample.p1_handsID,
        sample.p1_weaponID,
        sample.p1_weaponID,

        sample.p2_weaponID,
        sample.p2_hatID,
        sample.p2_amuletID,
        sample.p2_capeID,
        sample.p2_chestID,
        sample.p2_feetID,
        sample.p2_legsID,
        sample.p2_shieldID,
        sample.p2_handsID,
        sample.p2_weaponID,
        sample.p2_weaponID
    ]

    for item in equipmentList:
        if equipmentScrapeSucceded(item) == False:
            response["status"] = False
            response["Reason"] = "Equipement {} failed to load".format(item.id)
            return response

    response["status"] = True
    return response


validationFunctions = [
    validEquipment
]


class sampleOutput:
    def __init__(self, sample, winnerSample):
        self.sample = sample
        self.winnerSample = winnerSample

    def getMatchOutcome(self):
        if (self.winnerSample.winnerPlayer != None):
            return 1  # 1 = someone was killed
        else:
            return 0  # 0 = someone fled

    def getInitiator(self):
        if (self.getMatchOutcome == "1"):
            if (self.winnerSample.winnerPlayer == self.winnerSample.player1_id):
                return 1  # indicate player one won
            else:
                return 2
        else:
            if (self.winnerSample.fleerPlayer == self.winnerSample.player1_id):
                return 1  # indicate player one fled
            else:
                return 2

    def getdefStabBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.defStabBonus
        totalBonus += self.sample.p1_hatID.defStabBonus
        totalBonus += self.sample.p1_amuletID.defStabBonus
        totalBonus += self.sample.p1_capeID.defStabBonus
        totalBonus += self.sample.p1_chestID.defStabBonus
        totalBonus += self.sample.p1_feetID.defStabBonus
        totalBonus += self.sample.p1_legsID.defStabBonus
        totalBonus += self.sample.p1_shieldID.defStabBonus
        totalBonus += self.sample.p1_handsID.defStabBonus
        return totalBonus
    
    def getdefStabBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.defStabBonus
        totalBonus += self.sample.p2_hatID.defStabBonus
        totalBonus += self.sample.p2_amuletID.defStabBonus
        totalBonus += self.sample.p2_capeID.defStabBonus
        totalBonus += self.sample.p2_chestID.defStabBonus
        totalBonus += self.sample.p2_feetID.defStabBonus
        totalBonus += self.sample.p2_legsID.defStabBonus
        totalBonus += self.sample.p2_shieldID.defStabBonus
        totalBonus += self.sample.p2_handsID.defStabBonus
        return totalBonus

    def getdefSlashBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.defSlashBonus
        totalBonus += self.sample.p1_hatID.defSlashBonus
        totalBonus += self.sample.p1_amuletID.defSlashBonus
        totalBonus += self.sample.p1_capeID.defSlashBonus
        totalBonus += self.sample.p1_chestID.defSlashBonus
        totalBonus += self.sample.p1_feetID.defSlashBonus
        totalBonus += self.sample.p1_legsID.defSlashBonus
        totalBonus += self.sample.p1_shieldID.defSlashBonus
        totalBonus += self.sample.p1_handsID.defSlashBonus
        return totalBonus
    
    def getdefSlashBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.defSlashBonus
        totalBonus += self.sample.p2_hatID.defSlashBonus
        totalBonus += self.sample.p2_amuletID.defSlashBonus
        totalBonus += self.sample.p2_capeID.defSlashBonus
        totalBonus += self.sample.p2_chestID.defSlashBonus
        totalBonus += self.sample.p2_feetID.defSlashBonus
        totalBonus += self.sample.p2_legsID.defSlashBonus
        totalBonus += self.sample.p2_shieldID.defSlashBonus
        totalBonus += self.sample.p2_handsID.defSlashBonus
        return totalBonus

    def getdefCrushBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.defCrushBonus
        totalBonus += self.sample.p1_hatID.defCrushBonus
        totalBonus += self.sample.p1_amuletID.defCrushBonus
        totalBonus += self.sample.p1_capeID.defCrushBonus
        totalBonus += self.sample.p1_chestID.defCrushBonus
        totalBonus += self.sample.p1_feetID.defCrushBonus
        totalBonus += self.sample.p1_legsID.defCrushBonus
        totalBonus += self.sample.p1_shieldID.defCrushBonus
        totalBonus += self.sample.p1_handsID.defCrushBonus
        return totalBonus

    def getdefCrushBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.defCrushBonus
        totalBonus += self.sample.p2_hatID.defCrushBonus
        totalBonus += self.sample.p2_amuletID.defCrushBonus
        totalBonus += self.sample.p2_capeID.defCrushBonus
        totalBonus += self.sample.p2_chestID.defCrushBonus
        totalBonus += self.sample.p2_feetID.defCrushBonus
        totalBonus += self.sample.p2_legsID.defCrushBonus
        totalBonus += self.sample.p2_shieldID.defCrushBonus
        totalBonus += self.sample.p2_handsID.defCrushBonus
        return totalBonus

    def getdefMagicBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.defMagicBonus
        totalBonus += self.sample.p1_hatID.defMagicBonus
        totalBonus += self.sample.p1_amuletID.defMagicBonus
        totalBonus += self.sample.p1_capeID.defMagicBonus
        totalBonus += self.sample.p1_chestID.defMagicBonus
        totalBonus += self.sample.p1_feetID.defMagicBonus
        totalBonus += self.sample.p1_legsID.defMagicBonus
        totalBonus += self.sample.p1_shieldID.defMagicBonus
        totalBonus += self.sample.p1_handsID.defMagicBonus
        return totalBonus

    def getdefMagicBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.defMagicBonus
        totalBonus += self.sample.p2_hatID.defMagicBonus
        totalBonus += self.sample.p2_amuletID.defMagicBonus
        totalBonus += self.sample.p2_capeID.defMagicBonus
        totalBonus += self.sample.p2_chestID.defMagicBonus
        totalBonus += self.sample.p2_feetID.defMagicBonus
        totalBonus += self.sample.p2_legsID.defMagicBonus
        totalBonus += self.sample.p2_shieldID.defMagicBonus
        totalBonus += self.sample.p2_handsID.defMagicBonus
        return totalBonus

    

    def getdefRangeBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.defRangeBonus
        totalBonus += self.sample.p1_hatID.defRangeBonus
        totalBonus += self.sample.p1_amuletID.defRangeBonus
        totalBonus += self.sample.p1_capeID.defRangeBonus
        totalBonus += self.sample.p1_chestID.defRangeBonus
        totalBonus += self.sample.p1_feetID.defRangeBonus
        totalBonus += self.sample.p1_legsID.defRangeBonus
        totalBonus += self.sample.p1_shieldID.defRangeBonus
        totalBonus += self.sample.p1_handsID.defRangeBonus
        return totalBonus

    def getdefRangeBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.defRangeBonus
        totalBonus += self.sample.p2_hatID.defRangeBonus
        totalBonus += self.sample.p2_amuletID.defRangeBonus
        totalBonus += self.sample.p2_capeID.defRangeBonus
        totalBonus += self.sample.p2_chestID.defRangeBonus
        totalBonus += self.sample.p2_feetID.defRangeBonus
        totalBonus += self.sample.p2_legsID.defRangeBonus
        totalBonus += self.sample.p2_shieldID.defRangeBonus
        totalBonus += self.sample.p2_handsID.defRangeBonus
        return totalBonus

    def getattStabBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.attStabBonus
        totalBonus += self.sample.p1_hatID.attStabBonus
        totalBonus += self.sample.p1_amuletID.attStabBonus
        totalBonus += self.sample.p1_capeID.attStabBonus
        totalBonus += self.sample.p1_chestID.attStabBonus
        totalBonus += self.sample.p1_feetID.attStabBonus
        totalBonus += self.sample.p1_legsID.attStabBonus
        totalBonus += self.sample.p1_shieldID.attStabBonus
        totalBonus += self.sample.p1_handsID.attStabBonus
        return totalBonus


    def getattStabBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.attStabBonus
        totalBonus += self.sample.p2_hatID.attStabBonus
        totalBonus += self.sample.p2_amuletID.attStabBonus
        totalBonus += self.sample.p2_capeID.attStabBonus
        totalBonus += self.sample.p2_chestID.attStabBonus
        totalBonus += self.sample.p2_feetID.attStabBonus
        totalBonus += self.sample.p2_legsID.attStabBonus
        totalBonus += self.sample.p2_shieldID.attStabBonus
        totalBonus += self.sample.p2_handsID.attStabBonus
        return totalBonus

    def getattSlashBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.attSlashBonus
        totalBonus += self.sample.p1_hatID.attSlashBonus
        totalBonus += self.sample.p1_amuletID.attSlashBonus
        totalBonus += self.sample.p1_capeID.attSlashBonus
        totalBonus += self.sample.p1_chestID.attSlashBonus
        totalBonus += self.sample.p1_feetID.attSlashBonus
        totalBonus += self.sample.p1_legsID.attSlashBonus
        totalBonus += self.sample.p1_shieldID.attSlashBonus
        totalBonus += self.sample.p1_handsID.attSlashBonus
        return totalBonus
    
    def getattSlashBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.attSlashBonus
        totalBonus += self.sample.p2_hatID.attSlashBonus
        totalBonus += self.sample.p2_amuletID.attSlashBonus
        totalBonus += self.sample.p2_capeID.attSlashBonus
        totalBonus += self.sample.p2_chestID.attSlashBonus
        totalBonus += self.sample.p2_feetID.attSlashBonus
        totalBonus += self.sample.p2_legsID.attSlashBonus
        totalBonus += self.sample.p2_shieldID.attSlashBonus
        totalBonus += self.sample.p2_handsID.attSlashBonus
        return totalBonus

    def getattCrushBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.attCrushBonus
        totalBonus += self.sample.p1_hatID.attCrushBonus
        totalBonus += self.sample.p1_amuletID.attCrushBonus
        totalBonus += self.sample.p1_capeID.attCrushBonus
        totalBonus += self.sample.p1_chestID.attCrushBonus
        totalBonus += self.sample.p1_feetID.attCrushBonus
        totalBonus += self.sample.p1_legsID.attCrushBonus
        totalBonus += self.sample.p1_shieldID.attCrushBonus
        totalBonus += self.sample.p1_handsID.attCrushBonus
        return totalBonus

    def getattCrushBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.attCrushBonus
        totalBonus += self.sample.p2_hatID.attCrushBonus
        totalBonus += self.sample.p2_amuletID.attCrushBonus
        totalBonus += self.sample.p2_capeID.attCrushBonus
        totalBonus += self.sample.p2_chestID.attCrushBonus
        totalBonus += self.sample.p2_feetID.attCrushBonus
        totalBonus += self.sample.p2_legsID.attCrushBonus
        totalBonus += self.sample.p2_shieldID.attCrushBonus
        totalBonus += self.sample.p2_handsID.attCrushBonus
        return totalBonus

    def getattMagicBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.attMagicBonus
        totalBonus += self.sample.p1_hatID.attMagicBonus
        totalBonus += self.sample.p1_amuletID.attMagicBonus
        totalBonus += self.sample.p1_capeID.attMagicBonus
        totalBonus += self.sample.p1_chestID.attMagicBonus
        totalBonus += self.sample.p1_feetID.attMagicBonus
        totalBonus += self.sample.p1_legsID.attMagicBonus
        totalBonus += self.sample.p1_shieldID.attMagicBonus
        totalBonus += self.sample.p1_handsID.attMagicBonus
        return totalBonus

    def getattMagicBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.attMagicBonus
        totalBonus += self.sample.p2_hatID.attMagicBonus
        totalBonus += self.sample.p2_amuletID.attMagicBonus
        totalBonus += self.sample.p2_capeID.attMagicBonus
        totalBonus += self.sample.p2_chestID.attMagicBonus
        totalBonus += self.sample.p2_feetID.attMagicBonus
        totalBonus += self.sample.p2_legsID.attMagicBonus
        totalBonus += self.sample.p2_shieldID.attMagicBonus
        totalBonus += self.sample.p2_handsID.attMagicBonus
        return totalBonus

    def getattRangeBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.attRangeBonus
        totalBonus += self.sample.p1_hatID.attRangeBonus
        totalBonus += self.sample.p1_amuletID.attRangeBonus
        totalBonus += self.sample.p1_capeID.attRangeBonus
        totalBonus += self.sample.p1_chestID.attRangeBonus
        totalBonus += self.sample.p1_feetID.attRangeBonus
        totalBonus += self.sample.p1_legsID.attRangeBonus
        totalBonus += self.sample.p1_shieldID.attRangeBonus
        totalBonus += self.sample.p1_handsID.attRangeBonus
        return totalBonus

    def getattRangeBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.attRangeBonus
        totalBonus += self.sample.p2_hatID.attRangeBonus
        totalBonus += self.sample.p2_amuletID.attRangeBonus
        totalBonus += self.sample.p2_capeID.attRangeBonus
        totalBonus += self.sample.p2_chestID.attRangeBonus
        totalBonus += self.sample.p2_feetID.attRangeBonus
        totalBonus += self.sample.p2_legsID.attRangeBonus
        totalBonus += self.sample.p2_shieldID.attRangeBonus
        totalBonus += self.sample.p2_handsID.attRangeBonus
        return totalBonus

    def getstrBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.strBonus
        totalBonus += self.sample.p1_hatID.strBonus
        totalBonus += self.sample.p1_amuletID.strBonus
        totalBonus += self.sample.p1_capeID.strBonus
        totalBonus += self.sample.p1_chestID.strBonus
        totalBonus += self.sample.p1_feetID.strBonus
        totalBonus += self.sample.p1_legsID.strBonus
        totalBonus += self.sample.p1_shieldID.strBonus
        totalBonus += self.sample.p1_handsID.strBonus
        return totalBonus

    def getstrBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.strBonus
        totalBonus += self.sample.p2_hatID.strBonus
        totalBonus += self.sample.p2_amuletID.strBonus
        totalBonus += self.sample.p2_capeID.strBonus
        totalBonus += self.sample.p2_chestID.strBonus
        totalBonus += self.sample.p2_feetID.strBonus
        totalBonus += self.sample.p2_legsID.strBonus
        totalBonus += self.sample.p2_shieldID.strBonus
        totalBonus += self.sample.p2_handsID.strBonus
        return totalBonus

    def getrangeBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.rangeBonus
        totalBonus += self.sample.p1_hatID.rangeBonus
        totalBonus += self.sample.p1_amuletID.rangeBonus
        totalBonus += self.sample.p1_capeID.rangeBonus
        totalBonus += self.sample.p1_chestID.rangeBonus
        totalBonus += self.sample.p1_feetID.rangeBonus
        totalBonus += self.sample.p1_legsID.rangeBonus
        totalBonus += self.sample.p1_shieldID.rangeBonus
        totalBonus += self.sample.p1_handsID.rangeBonus
        return totalBonus

    def getrangeBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.rangeBonus
        totalBonus += self.sample.p2_hatID.rangeBonus
        totalBonus += self.sample.p2_amuletID.rangeBonus
        totalBonus += self.sample.p2_capeID.rangeBonus
        totalBonus += self.sample.p2_chestID.rangeBonus
        totalBonus += self.sample.p2_feetID.rangeBonus
        totalBonus += self.sample.p2_legsID.rangeBonus
        totalBonus += self.sample.p2_shieldID.rangeBonus
        totalBonus += self.sample.p2_handsID.rangeBonus
        return totalBonus

    def getmagicBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.magicBonus
        totalBonus += self.sample.p1_hatID.magicBonus
        totalBonus += self.sample.p1_amuletID.magicBonus
        totalBonus += self.sample.p1_capeID.magicBonus
        totalBonus += self.sample.p1_chestID.magicBonus
        totalBonus += self.sample.p1_feetID.magicBonus
        totalBonus += self.sample.p1_legsID.magicBonus
        totalBonus += self.sample.p1_shieldID.magicBonus
        totalBonus += self.sample.p1_handsID.magicBonus
        return totalBonus

    def getmagicBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.magicBonus
        totalBonus += self.sample.p2_hatID.magicBonus
        totalBonus += self.sample.p2_amuletID.magicBonus
        totalBonus += self.sample.p2_capeID.magicBonus
        totalBonus += self.sample.p2_chestID.magicBonus
        totalBonus += self.sample.p2_feetID.magicBonus
        totalBonus += self.sample.p2_legsID.magicBonus
        totalBonus += self.sample.p2_shieldID.magicBonus
        totalBonus += self.sample.p2_handsID.magicBonus
        return totalBonus

    def getprayerBonus_1(self):
        totalBonus = 0
        totalBonus += self.sample.p1_weaponID.prayerBonus
        totalBonus += self.sample.p1_hatID.prayerBonus
        totalBonus += self.sample.p1_amuletID.prayerBonus
        totalBonus += self.sample.p1_capeID.prayerBonus
        totalBonus += self.sample.p1_chestID.prayerBonus
        totalBonus += self.sample.p1_feetID.prayerBonus
        totalBonus += self.sample.p1_legsID.prayerBonus
        totalBonus += self.sample.p1_shieldID.prayerBonus
        totalBonus += self.sample.p1_handsID.prayerBonus
        return totalBonus

    def getprayerBonus_2(self):
        totalBonus = 0
        totalBonus += self.sample.p2_weaponID.prayerBonus
        totalBonus += self.sample.p2_hatID.prayerBonus
        totalBonus += self.sample.p2_amuletID.prayerBonus
        totalBonus += self.sample.p2_capeID.prayerBonus
        totalBonus += self.sample.p2_chestID.prayerBonus
        totalBonus += self.sample.p2_feetID.prayerBonus
        totalBonus += self.sample.p2_legsID.prayerBonus
        totalBonus += self.sample.p2_shieldID.prayerBonus
        totalBonus += self.sample.p2_handsID.prayerBonus
        return totalBonus

    def output(self):
        text = f"{self.sample.id},"\
                f"{self.sample.fightDuration},"\
                f"{self.sample.fightState},"\
                f"{self.getMatchOutcome()},"\
                f"{self.getInitiator()},"\
                f"{self.sample.p1_tileX},"\
                f"{self.sample.p1_tileY},"\
                f"{self.sample.p1_tileZ},"\
                f"{1 if self.sample.p1_isSkulled else 0},"\
                f"{self.sample.p1_healthLeft},"\
                f"{self.sample.player1.combatLevel},"\
                f"{self.sample.player1.strLevel},"\
                f"{self.sample.player1.attLevel},"\
                f"{self.sample.player1.defLevel},"\
                f"{self.sample.player1.rangeLevel},"\
                f"{self.sample.player1.magicLevel},"\
                f"{self.sample.player1.prayerLevel},"\
                f"{self.sample.player1.healthLevel},"\
                f"{self.getdefStabBonus_1()},"\
                f"{self.getdefSlashBonus_1()},"\
                f"{self.getdefCrushBonus_1()},"\
                f"{self.getdefMagicBonus_1()},"\
                f"{self.getdefRangeBonus_1()},"\
                f"{self.getattStabBonus_1()},"\
                f"{self.getattSlashBonus_1()},"\
                f"{self.getattCrushBonus_1()},"\
                f"{self.getattMagicBonus_1()},"\
                f"{self.getattRangeBonus_1()},"\
                f"{self.getstrBonus_1()},"\
                f"{self.getrangeBonus_1()},"\
                f"{self.getmagicBonus_1()},"\
                f"{self.getprayerBonus_1()},"\
                f"{self.sample.p2_tileX},"\
                f"{self.sample.p2_tileY},"\
                f"{self.sample.p2_tileZ},"\
                f"{1 if self.sample.p2_isSkulled else 0},"\
                f"{self.sample.p2_healthLeft},"\
                f"{self.sample.player2.combatLevel},"\
                f"{self.sample.player2.strLevel},"\
                f"{self.sample.player2.attLevel},"\
                f"{self.sample.player2.defLevel},"\
                f"{self.sample.player2.rangeLevel},"\
                f"{self.sample.player2.magicLevel},"\
                f"{self.sample.player2.prayerLevel},"\
                f"{self.sample.player2.healthLevel},"\
                f"{self.getdefStabBonus_2()},"\
                f"{self.getdefSlashBonus_2()},"\
                f"{self.getdefCrushBonus_2()},"\
                f"{self.getdefMagicBonus_2()},"\
                f"{self.getdefRangeBonus_2()},"\
                f"{self.getattStabBonus_2()},"\
                f"{self.getattSlashBonus_2()},"\
                f"{self.getattCrushBonus_2()},"\
                f"{self.getattMagicBonus_2()},"\
                f"{self.getattRangeBonus_2()},"\
                f"{self.getstrBonus_2()},"\
                f"{self.getrangeBonus_2()},"\
                f"{self.getmagicBonus_2()},"\
                f"{self.getprayerBonus_2()}"
        return text




invalidPlayers = []
invalidFights = 0

startTime = datetime.datetime.now()

fightIdList = getAllFightIds()
for fightIds in fightIdList:
    fightID = fightIds.fightID  # FightID of the current Fight

    # The end fight sample
    endFightSample = getEndFightByFightID(fightID)

    # Get all fight samples
    # Remove last element because its the first in the next fight
    fightSamples = getWholeFightById(fightID)[:-1]
    for sample in fightSamples:
        if (sample == None):
            invalidFights += 1
            continue

        if not sampleValidator.validate(sample):
            print("Invalid sample")
            continue

        isValid = True
        for func in validationFunctions:
            result = func(sample)

            if result["status"] == False:
                # print("ERROR {}(): sample {} - {}".format(
                #     result["funcName"], sample.id, result["Reason"]))
                isValid = False
        # print(sample.player1.combatLevel)
        outputSample = sampleOutput(sample, endFightSample)
        print(outputSample.output())
        continue
        # Print status
        timeElapsed = datetime.datetime.now() - startTime
        timeFormat = "%H:%M:%S"
        print("{:7s} Progress({}/{}) {} {}\t {:15s} vs {:15s} {} ".format(
            str(timeElapsed),
            fightID,
            len(fightIdList),
            fightID,
            sample.lineNumber,
            sample.player1_id,
            sample.player2_id,
            "valid" if isValid else "invalid"
        ))

print("Invalid Players:", invalidPlayers)
print("Invalid Fights:", invalidFights)
